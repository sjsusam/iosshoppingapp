const functions = require('firebase-functions');

const admin = require('firebase-admin');
var validator = require("email-validator");

const express = require('express')
const bodyParser = require('body-parser')
var stripe = require('stripe')('sk_test_51H8xevImQqB5USpBfX2NLIjuiVJq0RpMsNaAsel69OzLfhCWDELcd9YjUJq8nU1qJgwQhqjFU1co2BnNP52Luuid00u4YAnEz5')

const app = express()

admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.helloWorld = functions.https.onRequest((request, response) => {
   functions.logger.info("Hello logs!", {structuredData: true});
   response.send("Hello from Samod's Firebase!");
 });


 // Can be enhanced later on to not allow users from free- email providers to sign up
 exports.validateEmail = functions.https.onRequest((request, response) => {
    functions.logger.info("email Logs!", request.query);
    responseObj = {}
    emailId = request.query.email

   responseObj.isValidEmail = validator.validate(emailId)
    if(!responseObj.isValidEmail){
            responseObj.message ="Invalid email Format"
    }
    else{
        responseObj.message="Valid email format"
    }
   // response.send(responseObj);
    response.status(200).send({ data: responseObj })
  });
 

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: true
}))


app.get('/', function(req, res) {
	res.send('Samod here')
})
app.post('/charge', (req, res) => {

  var description = req.body.description
  var amount = req.body.amount
  var currency = req.body.currency
  var token = req.body.stripeToken

  console.log(req.body)

  stripe.charges.create({
    source: token,
    amount: amount,
    currency: currency,
    description: description

  }, function(err, charge) {
    if(err) {
      console.log(err, req.body)
      res.status(500).end()
    } else {
      console.log('success')
      res.status(200).send("success")
    }
  })

});


exports.payment = functions.https.onRequest(app);