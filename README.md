 A Breif Description of the code:

 A shopping app where users are able to browse products under different categories, view product details, login/ register, add to cart, manage user profiles and checkout using their Credit card.


Swift Programming: 

Have used UIKit for development of UI. 

Models:
There are in total 4 Models used depicting the 4 backbone of the Application

Category - For each Category displayed in Home Screen. Did a one time load of Categories to Firestore using a function run once

Item - Holds each product. Item has name, description, imageLinks and price. Everytime a new product is added to catalog a new Items object is created in firestore using this Item Model object. 
Basket - Every logged in user will have one Basket if he has added any product to it. Basket holds the products added to it and will have ownerID (userID of the user to whom basket belongs) and item List array in it. 

User - Represents each user who has registered. emailID, firstname, lastname, address and purchased Items IDs to show purchase history. 

Views: There are Multiple views for handling navigation back and forth between Category, Product list, Item Details View, Add Item View, Login, Profile, Purchased Items etc.  


Controllers: Controls the lifecycle of Views. Acts as Delegate for some functions. Category Collection view uses a cell and calculations to build the View. Product Listing uses Table View controller and dequeueReusableCell to view the items listing etc. 
Additional Libraries or SDKs used:

Stripe SDK - Stripe ios SDK gives a easy and secure way to authorize credit card and get the token in user’s device. This token is later used for payments.  

https://github.com/stripe/stripe-ios


JGProgressHUD - To display some nice consistent Heads up display. For Success and failure messages.
https://github.com/JonasGessner/JGProgressHUD


NVActivityIndicatorView - To display a good animation on loading or waiting activities. 
https://github.com/ninjaprox/NVActivityIndicatorView

Firebase Authentication
Firebase Authentication is used for complete Register and Login Functionality. Features used is Register, Verify, Forgot Password, Resend verification email etc. 

Firebase Cloud Function
There are 2 functions used. 
To connect to Stripe - Connection to Stripe for payment requires a private secret key to be used and hence should be connected from the backend server and not from UI. Use the cloud function for the same.
To validate email format - This cloud function validates the email format to be correct. This function can be extended later to filter out any unwanted free email domains. 


Cloud Firestore Database
There are 4 objects stored in the backend Database in Firestore. Category lists, Items, Basket and User.


Cloud Firebase Storage
This is used for storing images that are uploaded as part of Product images. 


Icons
I have used icons downloaded from the below website for my project. 

https://icons8.com/

firebasefunctions:
Firebasefunctions used in Swift program to make some external calls to Stripe Payments and for email format validation. 
