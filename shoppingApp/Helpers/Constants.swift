//
//  Constants.swift
//  Market
//
//  Created by samoshet on 7/11/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import Foundation

enum Constants {
    static let publishableKey = "pk_test_51H8xevImQqB5USpBPuk05moAd2BfxvAkHjfXSNuZR7ShTWFpDcY76MxoJ1x4sgEgNuuxSxXcHsyM1F16EuO5lrWN00Y24oJG3u"
    static let baseURLString = "https://us-central1-shoppingapp-e692d.cloudfunctions.net/"
    static let defaultCurrency = "usd"
    static let defaultDescription = "Purchase from iosShopping App"
}


//Firebase Headers
public let kUSER_PATH = "User"
public let kCATEGORY_PATH = "Category"
public let kITEMS_PATH = "Items"
public let kBASKET_PATH = "Basket"



//Category
public let kNAME = "name"
public let kIMAGENAME = "imageName"
public let kOBJECTID = "objectId"

//Item
public let kCATEGORYID = "categoryId"
public let kDESCRIPTION = "description"
public let kPRICE = "price"
public let kIMAGELINKS = "imageLinks"

//File References
public let kFILEREFERENCE = "gs://shoppingapp-e692d.appspot.com"

//Basket References
public let kOWNERID = "ownerId"
public let kITEMIDS = "itemIds"

//User fields
public let kEMAIL = "email"
public let kFIRSTNAME = "firstName"
public let kLASTNAME = "lastName"
public let kFULLNAME = "fullName"
public let kCURRENTUSER = "currentUser"
public let kFULLADDRESS = "fullAddress"
public let kONBOARD = "onBoard"
public let kPURCHASEDITEMIDS = "purchasedItemIds"
