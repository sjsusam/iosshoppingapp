//
//  FirebaseCollectionReference.swift
//  Market
//
//  Created by samoshet on 7/10/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import Foundation
import FirebaseFirestore

enum FCollectionReference: String {
    case User
    case Category
    case Items
    case Basket
}

func FirebaseReference(_ collectionReference: FCollectionReference) ->
    CollectionReference {
        return Firestore.firestore().collection(collectionReference.rawValue)
}
