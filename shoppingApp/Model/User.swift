//
//  User.swift
//  shoppingApp
//
//  Created by samoshet on 7/25/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import Foundation
import FirebaseAuth

class User{
    let objectId: String
    let email: String
    let firstName: String
    let lastName: String
    let fullName: String
    var purchasedItemIds: [String]
    
    var fullAddress: String?
    var onBoard: Bool
    
    
    //MARK: Initializers
    
    init(_objectId: String, _email: String, _firstName: String,  _lastName: String){
        objectId = _objectId
        email = _email
        firstName = _firstName
        lastName = _lastName
        fullName = _firstName + " " + _lastName
        fullAddress = ""
        onBoard = false
        purchasedItemIds=[]
    }
    
    init(_dictionary: NSDictionary){
        
        objectId = _dictionary[kOBJECTID] as! String
        
        if let mail = _dictionary[kEMAIL]{
            email = mail as! String
        } else {
            email = ""
        }
        
        if let fname = _dictionary[kFIRSTNAME]{
                  firstName = fname as! String
              } else {
                  firstName = ""
              }
        
          if let lname = _dictionary[kLASTNAME]{
                        lastName = lname as! String
                    } else {
                        lastName = ""
                    }
    
        fullName = firstName + " " + lastName
        
        if let fAddress = _dictionary[kFULLADDRESS]{
                        fullAddress = fAddress as! String
                    } else {
                        fullAddress = ""
                    }
        
        if let onB = _dictionary[kONBOARD]{
                        onBoard = onB as! Bool
                    } else {
                        onBoard = false
                    }
        
        if let pItemIds = _dictionary[kPURCHASEDITEMIDS]{
                        purchasedItemIds = pItemIds as! [String]
                    } else {
                        purchasedItemIds = []
                    }
    }
        
    class func currentId() -> String {
           return Auth.auth().currentUser!.uid
       }
       
       class func currentUser() -> User? {
           
           if Auth.auth().currentUser != nil {
               if let dictionary = UserDefaults.standard.object(forKey: kCURRENTUSER) {
                   return User.init(_dictionary: dictionary as! NSDictionary)
               }
           }
           
           return nil
       }
    
    //MARK: - Login func
    
    class func loginUserWith(email: String, password: String, completion: @escaping (_ error: Error?, _ isEmailVerified: Bool) -> Void) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (authDataResult, error) in
            
            if error == nil {
                
                if authDataResult!.user.isEmailVerified {
                    downloadUserFromFirestore(userId: authDataResult!.user.uid, email: email)
                    completion(error, true)
                } else {
                    
                    print("email is not varified")
                    completion(error, false)
                }
                
            } else {
                completion(error, false)
            }
        }
    }

    
    //MARK: - Register user
    
    class func registerUserWith(email: String, password: String, completion: @escaping (_ error: Error?) ->Void) {
        
   
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            
            completion(error)
            
            if error == nil {
                
                //send email verification
                authDataResult!.user.sendEmailVerification { (error) in
                    print("auth email verification error : ", error?.localizedDescription)
                }
            }
        }
    }
    
    //MARK: - Resend link methods

    class func resetPasswordFor(email: String, completion: @escaping (_ error: Error?) -> Void) {
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion(error)
        }
    }
    
    class func resendVerificationEmail(email: String, completion: @escaping (_ error: Error?) -> Void) {
        
        Auth.auth().currentUser?.reload(completion: { (error) in
            
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                print(" resend email error: ", error?.localizedDescription)
                
                completion(error)
            })
        })
    }
    
    class func logOutCurrentUser(completion: @escaping (_ error: Error?) -> Void) {
        
        do {
            try Auth.auth().signOut()
            UserDefaults.standard.removeObject(forKey: kCURRENTUSER)
            UserDefaults.standard.synchronize()
            completion(nil)

        } catch let error as NSError {
            completion(error)
        }
        
        
    }
    
}




//MARK: - Save user to firebase

func saveUserToFirestore(User: User) {
       
       FirebaseReference(.User).document(User.objectId).setData(userDictionaryFrom(user: User) as! [String : Any]) { (error) in
           
           if error != nil {
               print("error saving user \(error!.localizedDescription)")
           }
       }
   }


   func saveUserLocally(UserDictionary: NSDictionary) {
       
       UserDefaults.standard.set(UserDictionary, forKey: kCURRENTUSER)
       UserDefaults.standard.synchronize()
   }


//MARK: - DownloadUser

func downloadUserFromFirestore(userId: String, email: String) {
    
    FirebaseReference(.User).document(userId).getDocument { (snapshot, error) in
        
        guard let snapshot = snapshot else { return }
        
        if snapshot.exists {
            print("download current user from firestore")
            saveUserLocally(UserDictionary: snapshot.data()! as NSDictionary)
        } else {
            //there is no user, save new in firestore
            
            let user = User(_objectId: userId, _email: email, _firstName: "", _lastName: "")
            saveUserLocally(UserDictionary: userDictionaryFrom(user: user))
            saveUserToFirestore(User: user)
        }
    }
}


//MARK: Helper functions
func userDictionaryFrom(user: User) -> NSDictionary{
    
    return NSDictionary(objects: [user.objectId, user.email, user.firstName, user.lastName, user.fullName, user.fullAddress ?? "" , user.onBoard, user.purchasedItemIds], forKeys: [kOBJECTID as NSCopying, kEMAIL as NSCopying, kFIRSTNAME as NSCopying, kLASTNAME as NSCopying, kFULLNAME as NSCopying, kFULLADDRESS as NSCopying, kONBOARD as NSCopying,  kPURCHASEDITEMIDS as NSCopying ])
}


//MARK: - Update user

func updateCurrentUserInFirestore(withValues: [String : Any], completion: @escaping (_ error: Error?) -> Void) {
    
    
    if let dictionary = UserDefaults.standard.object(forKey: kCURRENTUSER) {
        
        let userObject = (dictionary as! NSDictionary).mutableCopy() as! NSMutableDictionary
        userObject.setValuesForKeys(withValues)
        
        FirebaseReference(.User).document(User.currentId()).updateData(withValues) { (error) in
            
            completion(error)
            
            if error == nil {
                saveUserLocally(UserDictionary: userObject)
            }
        }
    }
}
