//
//  Category.swift
//  Market
//
//  Created by samoshet on 7/11/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import Foundation
import UIKit

class Category {
    
    var id: String
    var name: String
    var image: UIImage?
    var imageName: String?
    
    init(_name: String, _imageName: String) {
        
        id = ""
        name = _name
        imageName = _imageName
        image = UIImage(named: _imageName)
    }
    
    init(_dictionary: NSDictionary) {
        id = _dictionary[kOBJECTID] as! String
        name = _dictionary[kNAME] as! String
        image = UIImage(named: _dictionary[kIMAGENAME] as? String ?? "")
    }
}

//MARK: Save Category function
   func saveCategoryToFirebase(_ category: Category) {
       
       let id = UUID().uuidString
       category.id = id
       
       FirebaseReference(.Category).document(id).setData(categoryDictionaryFrom(category) as! [String : Any])
   }


   //MARK: Helpers

   func categoryDictionaryFrom(_ category: Category) -> NSDictionary {
       
    return NSDictionary(objects: [category.id, category.name, category.imageName as Any], forKeys: [kOBJECTID as NSCopying, kNAME as NSCopying, kIMAGENAME as NSCopying])
   }

   //use only one time
   func createCategorySet() {

       let womenClothing = Category(_name: "Women's Clothing", _imageName: "womenCloth")
       let footWear = Category(_name: "Footwear", _imageName: "footWear")
       let electronics = Category(_name: "Electronics", _imageName: "electronics")
       let menClothing = Category(_name: "Men's Clothing & Accessories" , _imageName: "menCloth")
       let health = Category(_name: "Health & Beauty", _imageName: "health")
       let baby = Category(_name: "Gifts", _imageName: "gift")
       let home = Category(_name: "Home Appliances", _imageName: "home")
       let car = Category(_name: "Automobiles & Motorcyles", _imageName: "car")
       let luggage = Category(_name: "Luggage & bags", _imageName: "luggage")
       let jewelery = Category(_name: "Wines", _imageName: "wine")
       let hobby =  Category(_name: "Hobby, Sport, Traveling", _imageName: "hobby")
       let pet = Category(_name: "Chocolates", _imageName: "chocolate")
       let industry = Category(_name: "Pet products", _imageName: "pet")
       let garden = Category(_name: "Garden supplies", _imageName: "garden")
       let camera = Category(_name: "Cameras & Optics", _imageName: "camera")

       let arrayOfCategories = [womenClothing, footWear, electronics, menClothing, health, baby, home, car, luggage, jewelery, hobby, pet, industry, garden, camera]

       for category in arrayOfCategories {
           saveCategoryToFirebase(category)
       }

   }
   //MARK: Download category from firebase

   func downloadCategoriesFromFirebase(completion: @escaping (_ categoryArray: [Category]) -> Void) {
       
       var categoryArray: [Category] = []
       
       FirebaseReference(.Category).getDocuments { (snapshot, error) in
           
           guard let snapshot = snapshot else {
               completion(categoryArray)
               return
           }
           
           if !snapshot.isEmpty {
               
               for categoryDict in snapshot.documents {
                   print("Created New Category")
                   categoryArray.append(Category(_dictionary: categoryDict.data() as NSDictionary))
               }
           }
           
           completion(categoryArray)
       }
   }
