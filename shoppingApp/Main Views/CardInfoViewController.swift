//
//  CardInfoViewController.swift
//  shoppingApp
//
//  Created by samoshet on 7/25/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit
import Stripe


protocol CardInfoViewControllerDelegate {
    func didClickDone(_ token:STPToken)
    func didClickCancel()
}

class CardInfoViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButtonOutlet: UIButton!
    
    let paymentCardTextField = STPPaymentCardTextField()
    
    var delegate: CardInfoViewControllerDelegate?
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(paymentCardTextField)
        
        paymentCardTextField.delegate = self
        
        paymentCardTextField.translatesAutoresizingMaskIntoConstraints = false
        
        view.addConstraint(NSLayoutConstraint(item: paymentCardTextField,
                                              attribute: .top, relatedBy: .equal, toItem: doneButtonOutlet,
                                              attribute: .bottom, multiplier: 1, constant: 30))
        
        view.addConstraint(NSLayoutConstraint(item: paymentCardTextField,
                                                     attribute: .trailing, relatedBy: .equal, toItem: view,
                                                     attribute: .trailing, multiplier: 1, constant: -20))
        
        view.addConstraint(NSLayoutConstraint(item: paymentCardTextField,
                                                            attribute: .leading, relatedBy: .equal, toItem: view,
                                                            attribute: .leading, multiplier: 1, constant: 20))
        // Do any additional setup after loading the view.
    }
    
    
    //MARK: @IBActions
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        delegate?.didClickCancel()
     dismissView()
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
       processCard()
    }
    
    
    //MARK: Helpers
    
    private func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func processCard(){
        let cardParams = STPCardParams()
        cardParams.number = paymentCardTextField.cardNumber
        cardParams.expMonth = paymentCardTextField.expirationMonth
        cardParams.expYear = paymentCardTextField.expirationYear
        cardParams.cvc = paymentCardTextField.cvc
        
        STPAPIClient.shared().createToken(withCard: cardParams){ (token, error) in
            if error == nil {
                print("The token is:", token!)
                self.delegate?.didClickDone(token!)
                self.dismissView()
            }
            else {
                print("Error Processing Card token", error!.localizedDescription)
            }
    }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CardInfoViewController: STPPaymentCardTextFieldDelegate {
    
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        doneButtonOutlet.isEnabled = textField.isValid
    }
    
}
