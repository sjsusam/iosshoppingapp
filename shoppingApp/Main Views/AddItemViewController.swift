//
//  AddItemViewController.swift
//  Market
//
//  Created by samoshet on 7/11/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit
import Gallery
import JGProgressHUD
import NVActivityIndicatorView


class AddItemViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    //MARK: Vars
    var category: Category!
    var gallery: GalleryController!
    let hud = JGProgressHUD(style: .dark)
    
    var activityIndicator: NVActivityIndicatorView?
    
    var itemImages: [UIImage?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(category.id)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.width / 2 - 30, y: self.view.frame.height / 2 - 30, width: 60, height: 60), type: .ballPulse, color: #colorLiteral(red: 0.9998469949, green: 0.4941213727, blue: 0.4734867811, alpha: 1), padding: nil)
    }
    

    //MARK: IBActions
    
    @IBAction func doneBarButtonItemPressed(_ sender: Any) {
    
        dismissKeyboard()
        
        if fieldsAreCompleted(){
            print("We have values")
            saveToFirebase()
        }
        else{
            print("All values are required")
            //TODO: Show Error to the user
            self.hud.textLabel.text = "All Fields are mandatory!!"
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.show(in: self.view)
            self.hud.dismiss(afterDelay: 2.0)
        }
    }
    
    
    @IBAction func imageButtonPressed(_ sender: Any) {
        itemImages = []
        showImageGallery()
    }
    
    @IBAction func backgroundTapped(_ sender: Any) {
        dismissKeyboard()
        
    }
    
    //MARK: Helper functions
    
    private func fieldsAreCompleted() -> Bool{
        return(titleTextField.text != "" && priceTextField.text != "" && descriptionTextView.text != "")
    }
    
    private func dismissKeyboard(){
        self.view.endEditing(false)
    }
    
    //MARK: Save Item
    private func saveToFirebase(){
        
        showLoadingIndicator()
        let item = Item()
        
        item.id = UUID().uuidString
        item.name = titleTextField.text!
        item.categoryId = category.id
        item.description = descriptionTextView.text
        item.price = Double(priceTextField.text!)
        
        if itemImages.count > 0 {
            uploadImages(images: itemImages, itemId: item.id) { (imageLinkArray) in
                item.imageLinks = imageLinkArray
                
                saveItemToFirestore(item)
                self.hideLoadingIndicator()
                self.popTheView()
            }
            
        } else {
            saveItemToFirestore(item)
            popTheView()
        }
    }
    
    private func popTheView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:Activity Indicator
    
     private func showLoadingIndicator() {
           
           if activityIndicator != nil {
               self.view.addSubview(activityIndicator!)
               activityIndicator!.startAnimating()
           }
       }

       private func hideLoadingIndicator() {
           
           if activityIndicator != nil {
               activityIndicator!.removeFromSuperview()
               activityIndicator!.stopAnimating()
           }
       }
    
    
    private func showImageGallery(){
        self.gallery = GalleryController()
        self.gallery.delegate = self
        
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 6
        
        self.present(self.gallery, animated:true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddItemViewController: GalleryControllerDelegate{
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        if(images.count >  0){
            Image.resolve(images: images){ (resolvedImages) in
                self.itemImages = resolvedImages
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
    
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
       
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}
