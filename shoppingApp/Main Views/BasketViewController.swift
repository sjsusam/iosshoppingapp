//
//  BasketViewController.swift
//  shoppingApp
//
//  Created by samoshet on 7/19/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit
import JGProgressHUD
import Stripe

class BasketViewController: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet weak var totalItemsLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var checkoutButtonOutlet: UIButton!
    @IBOutlet weak var basketTotalPriceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    //MARK: - Vars
       var basket: Basket?
       var allItems: [Item] = []
       var purchasedItemIds : [String] = []
       
       let hud = JGProgressHUD(style: .dark)
       
    
    var totalPrice = 0
       
      
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = footerView

       
    }
   
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if(User.currentUser() != nil){
             loadBasketFromFirestore()
        }
        else {
            self.updateTotalLabels(true)
        }
       

    }
    
    //MARK: @IBAction
    @IBAction func checkoutButtonPressed(_ sender: Any) {

       if User.currentUser()!.onBoard {
            
            //tempFunction()
            
            //addItemsToPurchaseHistory(self.purchasedItemIds)
            //emptyTheBasket()
        //finishPayment(token: <#T##STPToken#>)
        
        showPaymentOptions()

        } else {
            showNotification(text: "Please complete your profile", isError: true)
           
        }
        
    }
    
    
    private func finishPayment(token: STPToken) {
           
           self.totalPrice = 0
           
           for item in allItems {
               purchasedItemIds.append(item.id)
               self.totalPrice += Int(item.price)
           }
           
           self.totalPrice = self.totalPrice * 100
           
           StripeClient.sharedClient.createAndConfirmPayment(token, amount: totalPrice) { (error) in
               
               if error == nil {
                   self.addItemsToPurchaseHistory(self.purchasedItemIds)
                   self.emptyTheBasket()
                   self.showNotification(text: "Payment Successful", isError: false)
               } else {
                self.showNotification(text: "Payment Error", isError: true)
                   print("error ", error!.localizedDescription)
               }
           }
       }

    private func showNotification(text: String, isError: Bool) {
        
        if isError {
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        } else {
            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        }
        
        self.hud.textLabel.text = text
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 2.0)
    }

    
    private func loadBasketFromFirestore() {
            
        downloadBasketFromFirestore(User.currentId()) { (basket) in
                
                self.basket = basket
                self.getBasketItems()
            }
        }
        
      private func getBasketItems() {
            
            if basket != nil {
                
                downloadItems(basket!.itemIds) { (allItems) in
                    
                    self.allItems = allItems
                    self.updateTotalLabels(false)
                    self.tableView.reloadData()
                }
            }
        }

    

    private func returnBasketTotalPrice() -> String {
           
           var totalPrice = 0.0
           
           for item in allItems {
               totalPrice += item.price
           }
           
           return "Total price: " + convertToCurrency(totalPrice)
       }

    
    private func updateTotalLabels(_ isEmpty: Bool) {
    
    if isEmpty {
        totalItemsLabel.text = "0"
        basketTotalPriceLabel.text = returnBasketTotalPrice()
     } else {
        totalItemsLabel.text = "\(allItems.count)"
        basketTotalPriceLabel.text = returnBasketTotalPrice()
    }
        
    checkoutButtonStatusUpdate()
        
        
    }
    
    private func checkoutButtonStatusUpdate(){
        
        checkoutButtonOutlet.isEnabled = allItems.count > 0
     
        if (checkoutButtonOutlet.isEnabled){
           checkoutButtonOutlet.backgroundColor = #colorLiteral(red: 0.9825763106, green: 0.493026793, blue: 0.4185336232, alpha: 1)
        }
        else{
            disableCheckoutButton()
        }
        
    }
    
    private func disableCheckoutButton(){
        checkoutButtonOutlet.isEnabled = false
        checkoutButtonOutlet.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
    
    private func removeItemFromBasket(itemId: String){
        
        for i in 0..<basket!.itemIds.count{
            if itemId == basket!.itemIds[i]{
                basket!.itemIds.remove(at: i)
                return
            }
            
        }
    }
    
    
    func tempFunction() {
           for item in allItems {
               print("we have ", item.id)
               purchasedItemIds.append(item.id)
           }
       }
       

       private func emptyTheBasket() {
           
           purchasedItemIds.removeAll()
           allItems.removeAll()
           tableView.reloadData()
           
           basket!.itemIds = []
           
           updateBasketInFirestore(basket!, withValues: [kITEMIDS : basket!.itemIds]) { (error) in
               
               if error != nil {
                   print("Error updating basket ", error!.localizedDescription)
               }
               
               self.getBasketItems()
           }
           
       }
       
       private func addItemsToPurchaseHistory(_ itemIds: [String]) {
           
           if User.currentUser() != nil {
               
               print("item ids , ", itemIds)
               let newItemIds = User.currentUser()!.purchasedItemIds + itemIds
               
               updateCurrentUserInFirestore(withValues: [kPURCHASEDITEMIDS : newItemIds]) { (error) in
                   
                   if error != nil {
                       print("Error adding purchased items ", error!.localizedDescription)
                   }
               }
           }
           
       }
    
    private func showPaymentOptions(){
        let alertController = UIAlertController(title: "Payment Options",message:"Choose preferred payment Option", preferredStyle: .actionSheet)
        
        let cardAction = UIAlertAction(title: "Pay with Card", style: .default) {(action) in
            
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "cardInfoVC") as! CardInfoViewController
            
            
            vc.delegate = self
            
            self.present(vc, animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alertController.addAction(cardAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK navigation
    
    private func showItemView(withItem: Item){
        let itemVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "itemView") as! ItemViewController
         
         itemVC.item = withItem
         
         self.navigationController?.pushViewController(itemVC, animated: true)
    }

      
}

    
extension BasketViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell
        
        cell.generateCell(allItems[indexPath.row])
        
        return cell
        
    }
    
    //MARK -- UITableView Delegate
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
       return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let itemToDelete = allItems[indexPath.row]
            allItems.remove(at: indexPath.row)
            tableView.reloadData()
            
            removeItemFromBasket(itemId: itemToDelete.id)
            
            updateBasketInFirestore(basket!, withValues: [kITEMIDS: basket!.itemIds]){(error) in
                
                if error != nil {
                    print("Erorr updating the basket", error!.localizedDescription)
                }
                
                self.getBasketItems()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            tableView.deselectRow(at: indexPath, animated: true)
            showItemView(withItem: allItems[indexPath.row])
        }
  
    
    
    
    
}


extension BasketViewController: CardInfoViewControllerDelegate{
    func didClickDone(_ token: STPToken) {
        print("We have a token:", token)
        finishPayment(token: token)
    }
    
    func didClickCancel() {
       showNotification(text: "Payment Cancelled", isError: true)
    }
    
    
}

