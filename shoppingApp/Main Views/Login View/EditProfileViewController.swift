//
//  EditProfileViewController.swift
//  shoppingApp
//
//  Created by samoshet on 7/25/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit
import JGProgressHUD

class EditProfileViewController: UIViewController {

    //MARK: IBOutlets
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    
    let hud = JGProgressHUD(style: .dark)
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserInfo()
       
    }
    

    
      //MARK: IBActions
      
    @IBAction func logoutButtonPressed(_ sender: Any) {
        logOutUser()
    }
    
    
    @IBAction func saveBarButtonPressed(_ sender: Any) {
        
            dismissKeyboard()
            
            if textFieldsHaveText() {
                
                let withValues = [kFIRSTNAME : nameTextField.text!, kLASTNAME : surnameTextField.text!, kFULLNAME : (nameTextField.text! + " " + surnameTextField.text!), kFULLADDRESS : addressTextField.text!]
                
                updateCurrentUserInFirestore(withValues: withValues) { (error) in
                    
                    if error == nil {
                        self.hud.textLabel.text = "Updated!"
                        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        self.hud.show(in: self.view)
                        self.hud.dismiss(afterDelay: 2.0)
                        
                    } else {
                        print("erro updating user ", error!.localizedDescription)
                       self.hud.textLabel.text = error!.localizedDescription
                       self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                       self.hud.show(in: self.view)
                       self.hud.dismiss(afterDelay: 2.0)
                    }
                }
                
            } else {
                hud.textLabel.text = "All fields are required!"
                hud.indicatorView = JGProgressHUDErrorIndicatorView()
                hud.show(in: self.view)
                hud.dismiss(afterDelay: 2.0)
                
            }

    }
    
    
    private func loadUserInfo() {
        
        if User.currentUser() != nil {
            let currentUser = User.currentUser()!
            
            nameTextField.text = currentUser.firstName
            surnameTextField.text = currentUser.lastName
            addressTextField.text = currentUser.fullAddress
        }
    }
    

    //MARK: - Helper funcs
    private func dismissKeyboard() {
        self.view.endEditing(false)
    }

    private func textFieldsHaveText() -> Bool {
        
        return (nameTextField.text != "" && surnameTextField.text != "" && addressTextField.text != "")
    }

    
    private func logOutUser() {
        User.logOutCurrentUser { (error) in
            
            if error == nil {
                print("logged out")
                self.navigationController?.popViewController(animated: true)
            }  else {
                print("error login out ", error!.localizedDescription)
            }
        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
