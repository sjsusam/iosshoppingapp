//
//  WelcomeViewController.swift
//  shoppingApp
//
//  Created by samoshet on 7/25/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit
import JGProgressHUD
import NVActivityIndicatorView
import FirebaseFunctions


class WelcomeViewController: UIViewController {
    
    //MARK: IB Outelets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var resendButtonOutlet: UIButton!
    
    
    //MARK: vars
    let hud = JGProgressHUD(style: .dark)
    var activityIdicator: NVActivityIndicatorView?
    
    lazy var functions = Functions.functions()
    
    
    
    //MARK: View lifecycle
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           
           activityIdicator = NVActivityIndicatorView(frame: CGRect(x: self.view.frame.width / 2 - 30, y: self.view.frame.height / 2 - 30, width: 60.0, height: 60.0), type: .ballPulse, color: #colorLiteral(red: 0.9998469949, green: 0.4941213727, blue: 0.4734867811, alpha: 1.0), padding: nil)
       }
 
    //MARK: IBActions

    @IBAction func loginButtonPressed(_ sender: Any) {
        print("login button")
        
        if textFieldsHaveText() {
                          loginUser()
                      } else {
                          hud.textLabel.text = "All fields are required"
                          hud.indicatorView = JGProgressHUDErrorIndicatorView()
                          hud.show(in: self.view)
                          hud.dismiss(afterDelay: 2.0)
                      }
     }
     
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
         print("cancel button")
        
        dismiss(animated: true, completion: nil)
        

    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
         print("Register button")
        
        if textFieldsHaveText() {
                      
                      registerUser()
                  } else {
                      hud.textLabel.text = "All fields are required"
                      hud.indicatorView = JGProgressHUDErrorIndicatorView()
                      hud.show(in: self.view)
                      hud.dismiss(afterDelay: 2.0)
                  }

    }
    
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
         print("Forgot button")
        if emailTextField.text != "" {
             resetThePassword()
         } else {
             hud.textLabel.text = "Please insert email!"
             hud.indicatorView = JGProgressHUDErrorIndicatorView()
             hud.show(in: self.view)
             hud.dismiss(afterDelay: 2.0)
         }
    }
    
    @IBAction func resendButtonPressed(_ sender: Any) {
         print("Resend button")
        
        User.resendVerificationEmail(email: emailTextField.text!) { (error) in
               
               print("error resending email", error?.localizedDescription)
           }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    //MARK: - Helpers
    
    private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
       
    
    private func textFieldsHaveText() -> Bool{
        
        return (emailTextField.text != "" && passwordTextField.text != "")
        
    }
    
    //MARK: Activity Indicator
       
       private func showLoadingIndicator(){
           self.view.addSubview(activityIdicator!)
           activityIdicator!.startAnimating()
           
       }
       
       private func hideLoadingIndicator(){
           
           if activityIdicator != nil {
               activityIdicator!.removeFromSuperview()
               activityIdicator!.stopAnimating()
           }
              
          }
    
    //MARK: Login User
   private func loginUser() {
        
        showLoadingIndicator()
        
        User.loginUserWith(email: emailTextField.text!, password: passwordTextField.text!) { (error, isEmailVerified) in
            
            if error == nil {
                
                if  isEmailVerified {
                    self.dismissView()
                    print("Email is verified")
                } else {
                    self.hud.textLabel.text = "Please Verify your email!"
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.show(in: self.view)
                    self.hud.dismiss(afterDelay: 2.0)
                    self.resendButtonOutlet.isHidden = false
                }
                
            } else {
                print("error loging in the user", error!.localizedDescription)
                self.hud.textLabel.text = error!.localizedDescription
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
            }
            
            
            self.hideLoadingIndicator()
        }
        
    }
    
    
    //MARK: Register User
    
    private func registerUser() {
           
           showLoadingIndicator()
        
           functions.httpsCallable("validateEmail?email="+emailTextField.text!).call([]) { (result, error) in
            if error == nil {
                if let isValid = (result?.data as? [String: Any])?["isValidEmail"] as? Bool {
                            let validEmail = isValid
                            
                            print("Value:"+isValid.description)
                    if(validEmail){
                     User.registerUserWith(email: self.emailTextField.text!, password: self.passwordTextField.text!) { (error) in
                                
                                if error == nil {
                                    self.hud.textLabel.text = "Varification Email sent!"
                                    self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                                    self.hud.show(in: self.view)
                                    self.hud.dismiss(afterDelay: 2.0)
                                } else {
                                    print("error registering", error!.localizedDescription)
                                    self.hud.textLabel.text = error!.localizedDescription
                                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                    self.hud.show(in: self.view)
                                    self.hud.dismiss(afterDelay: 2.0)
                             }
                                self.hideLoadingIndicator()
                            }
                    }
                    else{
                        print("Invalid Email Format")
                        self.hud.textLabel.text = "Email format is incorrect"
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.show(in: self.view)
                        self.hud.dismiss(afterDelay: 2.0)
                        self.hideLoadingIndicator()
                        
                    }
                }
            }
            else{
             if let error = error as NSError? {
               if error.domain == FunctionsErrorDomain {
                print("error registering", error.localizedDescription)
                self.hud.textLabel.text = "Error validating email"
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
               }
               // ...
             }
            
           }
           
        }
           
       }

       

    private func resetThePassword() {
        
        User.resetPasswordFor(email: emailTextField.text!) { (error) in
            
            if error == nil {
                self.hud.textLabel.text = "Reset password email sent!"
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
            } else {
                self.hud.textLabel.text = error!.localizedDescription
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.show(in: self.view)
                self.hud.dismiss(afterDelay: 2.0)
            }
        }
    }

}


