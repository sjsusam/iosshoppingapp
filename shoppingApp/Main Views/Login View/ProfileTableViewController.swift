//
//  ProfileTableViewController.swift
//  shoppingApp
//
//  Created by samoshet on 7/25/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController {
    
    
    //MARK - @IBOutlets
    @IBOutlet weak var finishRegistration: UIButton!
    @IBOutlet weak var purchaseHistory: UIButton!
    
    //MARK - vars
    var editBarButtonOutlet = UIBarButtonItem()
    
    //MARK - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           
        checkLoginStatus()
        checkOnboardingStatus()
       }

    // MARK: - Table view data source

   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    
    
    
    //MARK: TableView Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    //MARK - Helpers
    
    //MARK: - Helpers
    
    private func checkLoginStatus() {
        
        if User.currentUser() == nil {
            createRightBarButton(title: "Login")
        } else {
            createRightBarButton(title: "Edit")
        }
    }

    
    private func createRightBarButton(title: String) {
        
        editBarButtonOutlet = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(rightBarButtonItemPressed))
        
        self.navigationItem.rightBarButtonItem = editBarButtonOutlet
    }
    
    private func checkOnboardingStatus() {
        
        if User.currentUser() != nil {
            
            if User.currentUser()!.onBoard {
                finishRegistration.setTitle("Account is Active", for: .normal)
                finishRegistration.isEnabled = false
            } else {
                
                finishRegistration.setTitle("Finish registration", for: .normal)
                finishRegistration.isEnabled = true
                finishRegistration.tintColor = .red
            }
            purchaseHistory.isEnabled = true
            
        } else {
            finishRegistration.setTitle("Logged out", for: .normal)
            finishRegistration.isEnabled = false
            purchaseHistory.isEnabled = false
        }
    }



    private func showLoginView() {
        
        let loginView = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "loginView")
        
        self.present(loginView, animated: true, completion: nil)
    }
    
    private func goToEditProfile() {
       performSegue(withIdentifier: "profileToEditSegue", sender: self)
    }

    
    
    
    //MARK: - IBActions
    
    @objc func rightBarButtonItemPressed() {
        
        if editBarButtonOutlet.title == "Login" {
            showLoginView()
        } else {
           goToEditProfile()
        }
    }

    
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
