//
//  PurchasedHistoryTableViewController.swift
//  shoppingApp
//
//  Created by samoshet on 7/25/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit

class PurchasedHistoryTableViewController: UITableViewController {
    
    var itemArray: [Item] = []
    
    
    //MARK: View Lifecycle
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        loadItems()
        
    }
    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return itemArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell
        cell.generateCell(itemArray[indexPath.row])

        return cell
    }
    
    
    //Mark: loadItems
    
    private func loadItems() {
           
            downloadItems(User.currentUser()!.purchasedItemIds) { (allItems) in
               self.itemArray = allItems
               print("we have \(allItems.count) purchased items")
               self.tableView.reloadData()
           }
       }



   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
