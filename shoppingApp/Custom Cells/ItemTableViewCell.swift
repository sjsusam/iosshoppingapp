//
//  ItemTableViewCell.swift
//  Market
//
//  Created by samoshet on 7/18/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ItemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func generateCell(_ item: Item){
        nameLabel.text = item.name
        descriptionLabel.text = item.description
        priceLabel.text = convertToCurrency(item.price)
        priceLabel.adjustsFontSizeToFitWidth = true
               
      if item.imageLinks != nil && item.imageLinks.count > 0 {
              
              downloadImages(imageUrls: [item.imageLinks.first!]) { (images) in
                  self.ItemImageView.image = images.first as? UIImage
              }
          }

    }
 
  
  public func convertToCurrency(_ number: Double) -> String {
      
      let currencyFormatter = NumberFormatter()
      currencyFormatter.usesGroupingSeparator = true
      currencyFormatter.numberStyle = .currency
      currencyFormatter.locale = Locale.current
      
      return currencyFormatter.string(from: NSNumber(value: number))!
  }


}
