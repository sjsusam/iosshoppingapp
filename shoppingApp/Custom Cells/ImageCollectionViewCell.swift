//
//  ImageCollectionViewCell.swift
//  shoppingApp
//
//  Created by samoshet on 7/19/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func setupImageWith(itemImage: UIImage){
        imageView.image = itemImage
    }
}
