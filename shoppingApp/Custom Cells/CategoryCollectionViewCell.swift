//
//  CategoryCollectionViewCell.swift
//  Market
//
//  Created by samoshet on 7/10/20.
//  Copyright © 2020 Samod Shetty. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func generateCell(_ category: Category) {
        
        nameLabel.text = category.name
        imageView.image = category.image
    }
}
